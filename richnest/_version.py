VERSION = "0.95"

DESCRIPTION = """
RichnEst estimates population species richness from a small subsample.
"""

AUTHOR = "Johanna Schmitz, Christopher Schröder, Sven Rahmann"

EMAIL = "sven.rahmann@uni-saarland.de"
