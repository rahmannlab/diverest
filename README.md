# richnest: **Richn**ess **Est**imation

This method `richnest` can estimate the species richness of a whole population (100%) if the occupancy vector (count histogram) of a (small) subsample (e.g., 5%, 10%, 20%) is known.

The software is written in Python (>= 3.11) and requires additional modules, as specified in `environment.yml`.


## Installation

We highly recommend to use the [conda package manager](http://conda.pydata.org/miniconda.html) to install and configure the necessary Python environment.
This will allow you to keep the necessary dependencies independent from your system's Python installation. Also, no root/sudo access is required for `conda`.

After installing `conda`, create a Python environment for `richnest` and activate it as follows:

```bash
conda env create  # create environment from specification in environment.yml
conda activate richnest
```

To install `richnest` in development mode (so you can use edits immediately), go to the toplevel directory (where the `setup.py` file is) and run
```bash
pip install -e .
```

You should now be able to run the software; try printing the basic help information:
```bash
richnest --help
```

To deactivate the Python environment, run
```bash
conda deactivate
```

To completely remove the environment, use
```bash
conda remove --name richnest --all
```



## Basic Usage

This section describes how to run ```richnest``` for a typical use case.

You have a list of objects and count how many times each object i appears.
Let n_i be the number of occurrences ob object i.
Next you compute the occupancy numbers o_k = {i: n_i=k}.
The vector o = (o_1, o_2, ...) is the input to richnest.

(You can use the bam2occupancy tool to get occupancy vectors from BAM files; it will tell you how many fragments appear as a copy family of size 1, 2, 3, ...).

You will also need the target number N of objects, which must be larger than the number of objects that you have (the sum of the n_i).

Assuming that o = (65610, 14580, 1620, 90, 2) and N=1000000 and you want detailed (verbose) output, you can call
```bash
richnest -o 65610 14580 1620 90 2 -N 1000000 -v
```

To see all options, run
```bash
richnest --help
```
for all options.


## Contact

Please contact [Sven Rahmann](http://www.rahmannlab.de/people/rahmann)
with questions about the software,
or post bugs in the bug tracker of this repository.